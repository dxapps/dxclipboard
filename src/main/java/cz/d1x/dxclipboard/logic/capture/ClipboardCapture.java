package cz.d1x.dxclipboard.logic.capture;

/**
 * Interface for capturing clipboard contents.
 * <p>
 * Note that implementations automatically run in the context of calling thread.
 * If you want a separate thread for this capture, you should spawn it on your own.
 * Very likely implementations are used in the Swing applications so they don't need to spawn any loop-like thread.
 */
public interface ClipboardCapture {

    /**
     * Starts capturing clipboard contents.
     */
    void start();

    /**
     * Stops capturing clipboard contents.
     */
    void stop();
}
