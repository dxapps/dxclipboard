package cz.d1x.dxclipboard.logic.capture;

import java.util.ArrayList;
import java.util.List;

/**
 * Builder for final output that can be later emitted anywhere.
 * Also clients can use {@link EntryListener} to receive callbacks when there are any changes in entries.
 * Entries can be delimited by custom delimiter.
 */
public class OutputBuilder {

    private final List<String> entries = new ArrayList<>();
    private final List<EntryListener> listeners = new ArrayList<>();
    private String delimiter = "\n";


    /**
     * Adds a new string entry.
     * Also notifies all registered listeners previously added by {@link #addEntryListener(EntryListener)}.
     *
     * @param entry entry to be added
     */
    public void addStringEntry(String entry) {
        entries.add(entry);
        notifyListeners();
    }

    /**
     * Clears all entries in this builder.
     * Also notifies all registered listeners previously added by {@link #addEntryListener(EntryListener)}.
     */
    public void clearEntries() {
        entries.clear();
        notifyListeners();
    }

    /**
     * Sets a delimiter that should be put between entries in the final output.
     *
     * @param delimiter delimiter to be used
     */
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /**
     * Gets actual count of entries in the builder.
     *
     * @return current entries count
     */
    public int getEntriesCount() {
        return entries.size();
    }

    /**
     * Adds a new listener for entries change.
     *
     * @param listener listener to be added
     */
    public void addEntryListener(EntryListener listener) {
        listeners.add(listener);
    }

    /**
     * Builds the final output.
     *
     * @return final output
     */
    public String build() {
        return String.join(delimiter, entries);
    }

    private void notifyListeners() {
        listeners.forEach(l -> l.onEntriesChanged(this));
    }
}
