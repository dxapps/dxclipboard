package cz.d1x.dxclipboard.logic.capture;

/**
 * Listener that can be registered to receive callback when new entries are recorded.
 */
public interface EntryListener {

    /**
     * Callback when entries change.
     *
     * @param builder actual builder
     */
    void onEntriesChanged(OutputBuilder builder);
}
