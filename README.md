DXClipboard: Capture & Aggregate Clipboard
==========================================
Simple Java utility that captures clipboard "writes" over time (typically from CTRL-C) and aggregates them back to clipboard
as a single entry (delimited by custom delimiter).

I created this utility for my close friend that uses it in combination with other software 
to import some statistics.

Build
-----
Run ```gradle build```

Run Application
---------------
- Win: ```dxclipboard/dxclipboard.bat```
- Unix: ```dxclipboard/dxclipboard.sh```

Typical usage
-------------
Typical usage is to click START button to start capturing changes in clipboard.
When you think you captured all events, you STOP and AGGREGATE which populates all previously captured entries
back to the clipboard as a single value (delimited by given delimiter).

Sometimes it can be useful to START, then STOP for a while and then START again. 
Doing this will not remove any previously captured entries.
If you want to clear all previously captured entries, you can use CLEAR button.
